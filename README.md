# Bindworks CICD gitlab pipelines

This repository contains .gitlab-ci.yml includes for
Bindworks projects that should be used by all supported
project types:

- [Rules for all GitLab projects](#rules-for-all-gitlab-projects)
- [Rules for versioning](VERSIONING.md)
- [Flutter packages](#rules-for-flutter-packages)
- [Node packages](#rules-for-node-packages)

## Development process description

`master` branch is used for the most up to date version of the project. The release scripts *SHOULD* support `main` branch as well.

### Mainline bugfix development

When a feature is being developed, the developer **creates a branch out of `master`** branch and changes the code accordingly.

Upon completion (or at any earlier time), the developer **modifies `CHANGELOG.md` to contain record of humanly understandable description** of the change or feature prefixed with one of `BREAKING:` (for libraries), `MAJOR:` (for products) or `MINOR:` (see [VERSIONING.md](VERSIONING.md)). And **creates a merge request**. Please **do not use `PATCH:` for mainline development**.

After the merge request is approved, merge process bumps the version, releases the changelog and tags the result accordingly.

### Hotfix development

*TODO*

## Rules for Flutter packages

Should include `.gitlab-ci.yml` file in root containing:

  ```yml
  default:
    image: registry.gitlab.com/bindworks/cicd/docker-images/docker-flutter:3.0.0-7

  include:
  - project: bindworks/cicd/bw-gitlab-cicd
    ref: master
    file: 'cicd/flutter-package-gitlab-ci.yml'
  ```

  The `ref` attribute refers to the version number of the release process. If you wish to keep up to date automatically, put `master`.
  If you want to peg the project to a specific release process, put `version-X.Y.Z`.
  You can also peg the project to reference `latest-X.Y` or `latest-X` if you want to allow compatible changes to the release process.

## Rules for node packages

Should include `.gitlab-ci.yml` file in root containing:

  ```yml
  default:
    image: node:18

  include:
  - project: bindworks/cicd/bw-gitlab-cicd
    ref: master
    file: 'cicd/node-package-gitlab-ci.yml'
  ```

  The `ref` attribute refers to the version number of the release process. If you wish to keep up to date automatically, put `master`.
  If you want to peg the project to a specific release process, put `version-X.Y.Z`.
  You can also peg the project to reference `latest-X.Y` or `latest-X` if you want to allow compatible changes to the release process.

## Rules for all GitLab projects

- Settings/General/Merge requests

  - *Merge method:* Merge commit with semi-linear history
  - *Squash commits when merging:* Do not allow
  - *Merge checks:* Pipelines must succeed
  - *Merge commit message template:* (same as the GitLab default)

    ```text
    Merge branch '%{source_branch}' into '%{target_branch}'

    %{title}

    %{issues}

    See merge request %{reference}
    ```

  - *Squash commit message template:* (if squash commits are allowed, but they are certainly not preferred)

    ```text
    %{title}

    See merge request %{reference}
    ```

  - Should contain `CHANGELOG.md` starting with:

    ```md
    # Changelog
    
    All notable changes to this project will be documented in this file.

    The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
    this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

    ```

    For information on versioning, see `VERSIONING.md`.


## Rules for external packages

If you need to publish some external packages (for example because you need to reference some git fork in your library), you can use `external-flutter-package-gitlab-ci.yml` to do so.
Create empty repository with the same name as the external package in https://gitlab.com/bindworks/libs/external group and create and commit `.gitlab-ci.yml` inside with following content:

```yaml
variables:
  BW_EXTERNAL_URL: <REPLACE THIS - REPOSITORY GIT HTTP URL>
  BW_EXTERNAL_REF: <REPLACE THIS - REPOSITORY GIT REFERENCE>
  BW_EXTERNAL_PATH: <optional -- path to the root of the package to release relative to git root of the project>

default:
  image: registry.gitlab.com/bindworks/cicd/docker-images/docker-flutter:3.0.0-7

include:
- project: bindworks/cicd/bw-gitlab-cicd
  ref: master
  file: 'cicd/external-flutter-package-gitlab-ci.yml'
```
