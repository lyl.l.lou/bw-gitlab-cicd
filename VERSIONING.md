# General guide on versioning of Bindworks projects

There should be `CHANGELOG.md` file in repository which contains entries according to the format produced by [Cider](https://pub.dev/packages/cider#changelog). This convention is valid for all projects, not just flutter projects.

- [Rules on prefixing log messages](#rules-on-prefixing-log-messages)
- [Rules on merge request CHANGELOG.md](#rules-on-merge-request-changelogmd)

## Rules on prefixing log messages

In addition to *Cider* format, every entry in changelog should be prefixed with one of the strings:

- `BREAKING:` or `MAJOR:` for breaking or major changes in the functionality. This will bump version `X.Y.Z` to `X+1.0.0` upon release. Usually, libraries use **BREAKING** as the keyword, final products use **MAJOR**.

- `MINOR:` for minor changes in functionality, documentation, etc... This will bump version `X.Y.Z` to `X.Y+1.0`.

- `PATCH:` for hotfixes. Please **do not use PATCH** in standard process, reserve it for hotfixes, i.e. modifications to libraries or applications that are no longer in *master* branch, but need fixing before next major release is performed.

  *QUESTION:* should we enforce this in the release process? I.e. builds on `master` branch would always bump MAJOR or MINOR even if all log messages contain `PATCH:` prefix?

## Rules on merge request `CHANGELOG.md`

In a merge request, the changelog should contain first entry for all changes, additions and deletions that happened in the particular merge request prefixed with `Unreleased` revision, e.g.:

```md
## Unreleased
### Fixed
- MINOR: Fixed flickering bug on the login screen

### Added
- MAJOR: Support for SeaCat3
  * Android setup
    * Change `build.gradle` on line XY
    * Update `AndroidManifest.xml`
  * iOS
    * Update `Podfile` with XY
```

All root log messages (the one with BREAKING, MAJOR, MINOR, PATCH prefix) need to start start with `-`. Also they can have sublist (which can have another sublist etc.) but all the sublists needs to be pre fixed with `*`.

Relase process will bump version of the project accordingly after the merge request (X.Y.Z -> X+1.0.0 in this case) and change the title of the entry to contain the actual version with a link to the appropriate tag.
