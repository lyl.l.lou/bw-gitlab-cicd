# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Use **[Cider](https://pub.dev/packages/cider)** to manipulate this changelog. See &lt;VERSIONING.md&gt; for more information.

## [2.12.3] - 2022-11-01

### Changed

- PATCH: Allow lib or test directories to be missing

## [2.12.2] - 2022-10-31

### Added

- PATCH: Add ability to release non-root external packages

## [2.12.1] - 2022-09-29

### Fixed

- PATCH: Fix bug in determining the target branch name

## [2.12.0] - 2022-09-27

### Changed

- MINOR: Allow for multiline entries in ChangeLog.
  This should include:
  * paragraph splitting
  * inner bullet lists

## [2.11.0] - 2022-09-05

### Changed

- MINOR: Remove example before dry run publish in Dart packages

## [2.10.0] - 2022-09-05
### Added
- MINOR: Publishing of external packages

## [2.9.0] - 2022-09-05

### Changed

- MINOR: Ignoring generated localizations and icons files when running `flutter format`

## [2.8.0] - 2022-09-01
### Fixed
- MINOR: Remove example folder from root before publishing to JFrog

## [2.7.0] - 2022-08-19
### Fixed
- MINOR: Do not require generated dart sources to be formatted

## [2.6.0] - 2022-06-23
### Fixed
- MINOR: Fix skipping of flutter tests

## [2.5.0] - 2022-06-22
### Fixed
- MINOR: Use `dart pub publish` instead of flutter equivalent

## [2.4.0] - 2022-06-05
### Added
- MINOR: report CICD script version on the top

## [2.3.0] - 2022-06-05
### Fixed
- MINOR: do not run flutter tests if there are none

## [2.2.0] - 2022-06-01
### Fixed
- MINOR: do not create latest-\* tags for everything

## [2.1.0] - 2022-06-01
### Fixed
- MINOR: patch jFrog publishing

## [2.0.0] - 2022-06-01
### Added
- MINOR: support for node
- MINOR: small documentation changes
- MAJOR: moved from /libs/ to /cicd/
- MAJOR: remove version from flutter, image has to be specified in target project

## [1.1.0] - 2022-06-01
### Changed
- MINOR: simplify the templates and backreference
- MINOR: support dry run of release

## [1.0.0] - 2022-06-01
### Added
- MAJOR: first release of gitlab-cicd process

## [0.6.0] - 2022-06-01
### Fixed
- MINOR: tagging

## [0.5.0] - 2022-06-01
### Fixed
- MINOR: tagging

## [0.4.0] - 2022-06-01
### Fixed
- MINOR: support main branch
- MINOR: update `latest-` tags upon release
### Added
- MINOR: documentation on release process

## [0.3.0] - 2022-06-01
### Fixed
- MINOR: checkout to different path to not polute package publish and git repository

## [0.2.0] - 2022-06-01
### Fixed
- MINOR: fix changelog

## [0.1.2] - 2022-05-31
### Fixed
- MINOR: remove docker

## [0.1.1] - 2022-05-31
### Fixed
- PATCH: flutter release

## [0.1.0] - 2022-05-31
### Added
- MINOR: First attempt

[0.1.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-0.1.0
[0.1.1]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-0.1.1
[0.1.2]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-0.1.2
[0.2.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-0.2.0
[0.3.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-0.3.0
[0.4.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-0.4.0
[0.5.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-0.5.0
[0.6.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-0.6.0
[1.0.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-1.0.0
[1.1.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-1.1.0
[2.0.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.0.0
[2.1.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.1.0
[2.2.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.2.0
[2.3.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.3.0
[2.4.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.4.0
[2.5.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.5.0
[2.6.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.6.0
[2.7.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.7.0
[2.8.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.8.0
[2.9.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.9.0
[2.10.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.10.0
[2.11.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.11.0
[2.12.0]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.12.0
[2.12.1]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.12.1
[2.12.2]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.12.2
[2.12.3]: https://gitlab.com/bindworks/cicd/bw-gitlab-cicd/-/tags/version-2.12.3
