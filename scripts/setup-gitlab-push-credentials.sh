#!/usr/bin/env bash

#
# Usage: setup-gitlab-push-credentials.sh <environment-variable-name-with-gitlab-token>
#

if [ "$1" = "" ]; then
    echo "❗ Usage: $0 <environment-variable-name-with-gitlab-token>" >&2
    exit 1
fi

ACCESS_TOKEN="${!1}"

if [ "$ACCESS_TOKEN" = "" ]; then
    echo "❗ Variable $1 does not contain any GITLAB access token" >&2
    exit 1
fi

echo "🌟 Setting push credentials on repository..." >&2
PUSH_REPOSITORY_URL=$(echo "$CI_REPOSITORY_URL" | sed -re 's#https://[^@]*(@.*)#https://access_token:'"$ACCESS_TOKEN"'\1#')
git remote set-url --push origin "$PUSH_REPOSITORY_URL"
