#!/bin/sh

ALPINE_PKGS=""
DEBIAN_PKGS=""
INSTALL_SCHEDULED=no

executable_exists() {
    which "$1" >/dev/null
}

schedule_package_install() {
    ALPINE_PKGS="$ALPINE_PKGS $1"
    DEBIAN_PKGS="$DEBIAN_PKGS $1"
    INSTALL_SCHEDULED=yes
}

fail() {
    echo "❗$1"
    exit 1
}

executable_exists bash || schedule_package_install bash bash
executable_exists git || schedule_package_install git git
executable_exists awk || schedule_package_install gawk mawk

if [ $INSTALL_SCHEDULED = yes ]; then
    if executable_exists apt-get; then
        echo "setup-environment: installing prerequisities using apt-get: $DEBIAN_PKGS"
        apt-get update && apt-get install -y $DEBIAN_PKGS
    elif executable_exists apk; then
        echo "setup-environment: installing prerequisities using apk: $DEBIAN_PKGS"
        apk add $ALPINE_PKGS
    fi
else
    echo "setup-environment: no prerequisities missing"
fi

git config --global user.email "cicd@bindworks.eu"
git config --global user.name "Bindworks CI/CD"
