#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
source "$SCRIPT_DIR/common.inc.sh"

cicd_check_title "flutter pub get"
flutter pub get --quiet

if [ -d "./test" ] && [ "$(find ./test -type f -name \*_test.dart | wc -l)" -gt 0 ]; then
    cicd_check_title "flutter test"
    flutter test && cicd_accumulate_success "Flutter tests passed" || cicd_accumulate_error "Flutter tests failed"
fi

cicd_check_title "flutter analyze"
flutter analyze && cicd_accumulate_success "Flutter analyze passed" || cicd_accumulate_error "Flutter analyze failed"

cicd_check_title "flutter format"
PRESENT_DIRECTORIES="$(for DIRNAME in lib test; do if [ -d $DIRNAME ]; then echo $DIRNAME; fi; done)"
if [ -n "$PRESENT_DIRECTORIES" ]; then
  find $PRESENT_DIRECTORIES -name \*.dart | \
    grep -v '\.freezed\.dart$' | \
    grep -v '\.g\.dart$' | \
    grep -v 'localizations.*\.dart$' | \
    grep -v '_icons\.dart$' | \
    xargs flutter format -l 100 --set-exit-if-changed && cicd_accumulate_success "Flutter format passed" || cicd_accumulate_error "Flutter format not final, please run flutter format, commit and push"
fi

if [ -f flutter-stylizer.yaml ]; then
    cicd_check_title "flutter stylizer"
    flutter-stylizer -d ./... && cicd_accumulate_success "Flutter stylizer passed" || cicd_accumulate_error "Flutter stylizer not final, please run flutter-stylizer, commit and push"
fi

cicd_check_title "changelog messages"
cicd_check_changelog_not_empty
cicd_check_changelog_bumpy

cicd_check_title "dart pub publish"

REAL_DIR="$(pwd)"
PUBLISHING_COPY="$(mktemp -d -t publishing-copy-XXXXXX)"
cp -r . "$PUBLISHING_COPY"
cd "$PUBLISHING_COPY"
git rm -rq example || true
rm -rf example
dart pub publish --dry-run && cicd_accumulate_success "Dart pub --dry-run passed" || cicd_accumulate_error "Dart pub --dry-run failed"
cd "$REAL_DIR"

cicd_checks_finished
