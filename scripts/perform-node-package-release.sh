#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
source "$SCRIPT_DIR/common.inc.sh"

TARGET_BRANCH="$(cicd_find_target_branch)"
BUMP="$(cicd_determine_bump)" || { echo "🧑‍🏭 Changelog does not contain BUMP instructions, not releasing."; exit 0; }

cicd_step_title "Version bump: $BUMP"
npm config set git-tag-version false
npm version $BUMP
git add package.json
VERSION="$(node -e 'console.log(require("./package.json").version)')"

cicd_step_title "Release changelog"
cicd_release_changelog "$VERSION"
git add CHANGELOG.md

cicd_step_title "Commit back to branch $TARGET_BRANCH"
cicd_git_commit_and_push_to_target_branch "$VERSION"
cicd_git_tag_version_and_push "$VERSION"

cicd_step_title "Build and publish to jFrog"
skip_on_dryrun npm ci
skip_on_dryrun npm run build
skip_on_dryrun npm publish .
