set -e

COMMON_SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
COMMON_SCRIPT_VERSION="$( cat "$COMMON_SCRIPT_DIR/../Version" )"
COMMON_SCRIPT_VERSION_NAME="$COMMON_SCRIPT_VERSION"

if [ "$BW_CICD_VERSION_REF" != "" ]; then
    COMMON_SCRIPT_VERSION_NAME="$COMMON_SCRIPT_VERSION_NAME (ref $BW_CICD_VERSION_REF)"
fi

# == pure utility functions ==

repeat_string() {
    local char="$1"
    local count=$2
	for i in $(seq $count); do echo -n "$char"; done
}

skip_on_dryrun() {
    if [ "$BW_CICD_DRYRUN" = "" ]; then
        "${@}"
    else
        echo -ne "${COLOR_EMP}DRY RUN SKIPPING EXECUTION OF:${COLOR_END} "
        echo "${@}"
    fi
}

# == visual functions ==

cicd_check_title() {
    cicd_banner "CHECK: $1"
}

cicd_step_title() {
    local dryrun=""
    if [ "$BW_CICD_DRYRUN" != "" ]; then
        dryrun="(DRY-RUN) "
    fi
    cicd_banner "${dryrun}STEP: $1"
}

cicd_accumulate_error() {
    local entry="❌ $COLOR_FAIL$1"
    CDCI_CHECKS_RESULTS+=("$entry")
    CDCI_ERRORS_COUNT=$((CDCI_ERRORS_COUNT+1))
    echo -e "$entry$COLOR_END"
}

cicd_accumulate_success() {
    local entry="✅ $COLOR_SUCCESS$1"
    CDCI_CHECKS_RESULTS+=("$entry")
    echo -e "$entry$COLOR_END"
}

COLOR_BANNER="\e[0;33m"
COLOR_BANNER_MSG="\e[1;33m"
COLOR_SUCCESS="\e[1;32m"
COLOR_FAIL="\e[1;31m"
COLOR_EMP="\e[1;34m"
COLOR_END="\e[0m"

CDCI_ERRORS_COUNT=0
declare -a CDCI_CHECKS_RESULTS

cicd_banner() {
    local msg=$(printf %120s "$1")
    local length="$(echo -n "$msg" | wc -m)"
    local bars="$(repeat_string ═ $length)"
    local spaces="$(repeat_string " " $length)"
    local bottom_left=$(if [ "$2" = "1" ]; then echo -n "╞"; else echo -n "╘"; fi)
    echo -e "$COLOR_BANNER╒═════$bars═════╕$COLOR_END"
    echo -e "$COLOR_BANNER│     $spaces     │$COLOR_END"
    echo -e "$COLOR_BANNER│     $COLOR_BANNER_MSG$msg$COLOR_BANNER     │$COLOR_END"
    echo -e "$COLOR_BANNER│     $spaces     │$COLOR_END"
    echo -e "$COLOR_BANNER$bottom_left═════$bars═════╛$COLOR_END"
}

cicd_checks_finished() {
    local check
    cicd_banner "SUMMARY" 1
    for check in "${CDCI_CHECKS_RESULTS[@]}"; do
        echo -e "$COLOR_BANNER│ $COLOR_BANNER_MSG$check$COLOR_END" >&2
    done
    if [ $CDCI_ERRORS_COUNT -gt 0 ]; then
        echo "❗ Please fix the errors and commit."
        exit 1
    fi
    CDCI_CHECKS_RESULTS=()
}

# == changelog functions ==

cicd_extract_unreleased_changelog() {
    awk -- '
/^##[^#]+/ { in_unreleased = 0; }
/^##[^#]*Unreleased/ { in_unreleased = 1; }
in_unreleased == 1 && !/^#/ && !/^\s*$/ { print; }
    ' CHANGELOG.md
}

cicd_release_changelog() {
    local version="$1"
    local url="$CI_PROJECT_URL/-/tags/version-$version"
    local date="$(date +%Y-%m-%d)"
    awk -- '
/^##[^#]*Unreleased/ { print "## ['"$version"'] - '"$date"'"; next }
                     { print; }
END                  { print "['"$version"']: '"$url"'" }
    ' CHANGELOG.md > CHANGELOG.md.new
    mv -f CHANGELOG.md.new CHANGELOG.md
}

cicd_check_changelog_not_empty() {
    local total_count=$(cicd_extract_unreleased_changelog | grep -v '^\s\+' | wc -l)
    if [ "$total_count" -lt 1 ]; then
        cicd_accumulate_error "Changelog does not contain any entries"
    else
        cicd_accumulate_success "Changelog contains unreleased entries"
    fi
}

cicd_check_changelog_bumpy() {
    # All lines which begin on the first column must start with "- " followed by the bump spec
    # There can be lines starting with whitespace anywhere
    # This allows to have multiline entries in ChangeLog
    local total_count=$(cicd_extract_unreleased_changelog | grep -v '^\s\+' | wc -l)
    local total_correct_count=$(cicd_extract_unreleased_changelog | grep -c '^- \(BREAKING\|MAJOR\|MINOR\|PATCH\):')
    if [ "$total_count" -lt 1 ]; then
        cicd_accumulate_error "Changelog does not contain any entries"
    elif [ "$total_count" -ne "$total_correct_count" ]; then
        cicd_accumulate_error "All Changelog entries must start with BREAKING:, MAJOR:, MINOR: or PATCH:"
    else
        cicd_accumulate_success "Changelog entries all start with BREAKING, MAJOR, MINOR or PATCH"
    fi
}

cicd_determine_bump() {
    if [ $(cicd_extract_unreleased_changelog | grep -c '^- \(BREAKING\|MAJOR\):') -gt 0 ]; then
        echo major
    elif [ $(cicd_extract_unreleased_changelog | grep -c '^- MINOR:') -gt 0 ]; then
        echo minor
    elif [ $(cicd_extract_unreleased_changelog | grep -c '^- PATCH:') -gt 0 ]; then
        echo patch
    else
        return 1
    fi
    return 0
}

cicd_cut_version() {
    local version="$1"
    local cut="$2"
    local oifs="$IFS"
    IFS="."
    read major minor patch <<<"$version"
    IFS="$oifs"
    if [ "$cut" = "major" ]; then
        echo "$major"
    elif [ "$cut" = "minor" ]; then
        echo "$major.$minor"
    else
        false
    fi
}

cicd_bump_version() {
    local version="$1"
    local bump="$2"
    local oifs="$IFS"
    local major
    local minor
    local patch
    local oifs="$IFS"
    IFS="."
    read major minor patch <<<"$version"
    IFS="$oifs"
    if [ "$bump" = "major" ]; then
        major=$(($major+1))
        minor=0
        patch=0
    elif [ "$bump" = "minor" ]; then
        minor=$(($minor+1))
        patch=0
    elif [ "$bump" = "patch" ]; then
        patch=$(($patch+1))
    fi
    echo "$major.$minor.$patch"
}

# == git functions ==

cicd_find_merge_request() {
    git log -1 | sed -nr '/bindworks\/\S+![0-9]+/ {s#.*(bindworks/\S+![0-9]+).*#\1#;p}'
}

cicd_find_target_branch() {
    local target_branch

    target_branch="${1}"
    target_branch="${TARGET_BRANCH:-$CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
    target_branch="${TARGET_BRANCH:-$CI_COMMIT_BRANCH}"
    target_branch="${TARGET_BRANCH:-$CI_DEFAULT_BRANCH}"
    target_branch="${TARGET_BRANCH:-master}"

    echo "$target_branch"
}

cicd_construct_release_commit_message() {
    local version="$1"
    local merge_request="$(cicd_find_merge_request)"

    local message="[ci skip] Release $version"
    if [ "$merge_request" != "" ]; then
        message="$message Merge Request $merge_request"
    fi

    echo "$message"
}

cicd_git_commit_and_push_to_target_branch() {
    local version="$1"
    local message="$(cicd_construct_release_commit_message "$version")"
    local target_branch="$(cicd_find_target_branch)"
    echo "🌟 Committing with '$message' and pushing to target brach $target_branch..."
    git diff --color --cached
    git commit -v -m "$message"
    skip_on_dryrun git push origin "HEAD:$target_branch"
}

cicd_git_tag_version_and_push() {
    local version="$1"
    echo "🌟 Tagging with 'version-$version' and pushing..."
    git tag "version-$version"
    skip_on_dryrun git push origin "version-$version" 
}

cicd_git_tag_latest_and_push() {
    local version="$1"
    local minor_version="$(cicd_cut_version "$version" minor)"
    local major_version="$(cicd_cut_version "$version" major)"
    echo "🌟 Tagging with 'latest-$minor_version' and 'latest-$major_version' and pushing..."
    git tag -f "latest-$minor_version" 
    git tag -f "latest-$major_version"
    skip_on_dryrun git push -f origin "latest-$minor_version" "latest-$major_version"
}

echo -e "🌟 ${COLOR_EMP}Bindworks CICD scripts $COMMON_SCRIPT_VERSION_NAME$COLOR_END"
