#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
source "$SCRIPT_DIR/../scripts/common.inc.sh"

TARGET_BRANCH="$(cicd_find_target_branch)"
BUMP="$(cicd_determine_bump)" || { echo "🧑‍🏭 Changelog does not contain BUMP instructions, not releasing."; exit 0; }

cicd_step_title "Version bump: $BUMP"
CURRENT_VERSION="$(cat Version)"
VERSION="$(cicd_bump_version "$CURRENT_VERSION" "$BUMP")"
echo "🌟 New version: $VERSION"
echo "$VERSION" > Version
git add Version

cicd_step_title "Release changelog"
cicd_release_changelog "$VERSION"
git add CHANGELOG.md

cicd_step_title "Patch CICD templates to contain the correct BW_CICD_VERSION_REF"
(
    cd cicd;
    for FN in *.yml; do
        sed -e 's#BW_CICD_VERSION_REF:.*#BW_CICD_VERSION_REF: "version-'"$VERSION"'"#' < "$FN" > "$FN.new"
        mv -f "$FN.new" "$FN"
        git add "$FN"
    done
)

cicd_step_title "Commit back to branch $TARGET_BRANCH"
cicd_git_commit_and_push_to_target_branch "$VERSION"
cicd_git_tag_version_and_push "$VERSION"
cicd_git_tag_latest_and_push "$VERSION"
