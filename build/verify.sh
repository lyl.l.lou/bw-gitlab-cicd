#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
source "$SCRIPT_DIR/../scripts/common.inc.sh"

cicd_check_title "changelog messages"
cicd_check_changelog_not_empty
cicd_check_changelog_bumpy

cicd_checks_finished
